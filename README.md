Python code for project group. Code is build to participate Kaggle competition. 
https://www.kaggle.com/c/sf-crime

Idea of this project is to learn deep learning principles fast with Keras. 

NOTICE: This code is not ment to be maintained. I wrote this as spike solution. 
That is why I did not spend time to clean it. Making clean code is time 
consuming process.

HOW TO RUN TRAINING AND PREDICTION:

This should be streamlined.

* Train boosting by running file train_boosted.py
* Predict Boosting by running file predict_boosted.py
* Finalize prediction file before sending it to Kaggle by running Scala object 
ParsePredictionFromRawVec
