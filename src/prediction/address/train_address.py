import numpy as np

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory
from prediction.utils.data_factory import DataFactory

np.random.seed(1337)  # for reproducibility

number_of_files = 1
limit = 0

variables = LocalVariables()
data_factory = DataFactory()

for x in range(0, number_of_files):
    print("file ", x, "/", number_of_files - 1)
    train_data, train_label, test_data, test_label, columns, lines = data_factory.get_address_data(limit, x)

    modelFactory = ModelFactory()
    model = modelFactory.get_address_pred_model(columns)
    if x > 0:
        model.load_weights(variables.address_weights(limit))

    early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
    checkpoint = ModelCheckpoint(variables.address_weights(limit), monitor='val_loss', verbose=0, save_best_only=False, mode='auto')
    model.fit(train_data, train_label, nb_epoch=1, batch_size=32, validation_split=0.1, callbacks=[checkpoint, early_stop])

    loss_and_metrics = model.evaluate(test_data, test_label, batch_size=32)
    print(loss_and_metrics)
    print(x, "file done")
