import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory


def get_write_type(file_nr):
    if file_nr == 0:
        return 'w'
    else:
        return 'a'

np.random.seed(1337)  # for reproducibility

number_of_files = 18
limit = 100

variables = LocalVariables()
modelFactory = ModelFactory()
data_factory = DataFactory()

for file_nr in range(0, number_of_files):
    print("starting file number", file_nr, "/", number_of_files - 1)
    test_data, ids, column_count, data_lines = data_factory.get_test_address(limit, file_nr)
    model = modelFactory.get_address_pred_model(column_count)
    model.load_weights(variables.address_weights(limit))

    classes = model.predict_classes(test_data, batch_size=32)

    if len(ids) != len(classes):
        raise NameError('Ids and classes must be column to column match with each other ' + str(ids[0]))

    with open(variables.prediction_raw, get_write_type(file_nr)) as result_file:
        total_size = len(ids)

        for result_nr in range(0, total_size):
            result_file.write(str(ids[result_nr][0]) + ";" + str(classes[result_nr]) + "\n")
    result_file.closed

