import numpy as np
from prediction.variables import LocalVariables


class PredictionService(object):

    @staticmethod
    def save_predictions(ids, proba):
        variables = LocalVariables()
        if len(ids) != len(proba):
            raise NameError('Ids and proba must be column to column match with each other')

        result_table = np.concatenate([ids.T, proba.T]).T
        np.savetxt(variables.predictions_vec, result_table, delimiter=',', fmt=["%-1u"] + ["%1.8f",]*39)