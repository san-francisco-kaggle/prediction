from prediction.variables import LocalVariables

import csv
import numpy as np


class DataFactory(object):
    variables = LocalVariables()
    header_line_count = 1

    def get_test_coord_and_time(self):
        coord_data, coord_ids, coord_column_count, coord_data_lines = self.load_data(self.variables.test_data_coord)
        time_data, time_ids, time_column_count, time_data_lines = self.load_data(self.variables.test_data_time)
        for x in range(0, len(coord_ids)):
            if np.argmax(coord_ids[x]) != np.argmax(time_ids[x]):
                raise NameError('Ids must be identical')

        return coord_ids, coord_data_lines, coord_data, coord_column_count, time_data, time_column_count

    def get_coords_and_time(self):
        train_size = 200000
        test_size = 50
        train_indexes, test_indexes = \
            self.get_indexes(train_size, test_size, self.variables.train_data_time)

        traindt,trainlt, testdt, testlt, columnst, linest = \
            self.get_data_by_indexes(self.variables.train_data_time, train_indexes, test_indexes)

        traindc,trainlc, testdc, testlc, columnsc, linesc = \
            self.get_data_by_indexes(self.variables.train_data_coord, train_indexes, test_indexes)

        for x in range(0, len(traindc) - 1):
            if np.argmax(trainlt[x]) != np.argmax(trainlc[x]):
                raise NameError('Labels must be identical!')

        train_labels = trainlt # == trainlc
        test_labels = testlt # == testlc
        return train_labels, traindt, traindc, test_labels, testdt, testdc, columnst, columnsc

    #todo: Propably not used anymore
    def get_data_indexes_random_sample(self, target_file):
        total_data_lines = self.get_data_line_count(target_file)
        return np.random.choice(total_data_lines, total_data_lines, replace=False)

    def get_data_line_count(self, target_file):
        def file_len():
            with open(target_file) as f:
                for i, l in enumerate(f):
                    pass
            return i + 1
        return file_len() - self.header_line_count

    def get_indexes(self, train_size, test_size, target_file):
        total_data_lines = self.get_data_line_count(target_file)
        if train_size == 0:
            train_size = total_data_lines - test_size
        elif total_data_lines < train_size + test_size - 1:
            raise NameError('More indexes requested than lines in code')

        used_indexes = np.random.choice(total_data_lines, train_size + test_size, replace=False)
        train_indexes = used_indexes[:train_size]
        test_indexes = used_indexes[train_size:train_size + test_size]
        return train_indexes, test_indexes

    def get_data_by_indexes(self, file_name, train_indexes, test_indexes):
        data, first_column_indexes, data_column_count, total_data_lines = self.load_data(file_name)

        train_data = data[train_indexes]
        train_label = first_column_indexes[train_indexes]
        test_data = data[test_indexes]
        test_label = first_column_indexes[test_indexes]
        return train_data, train_label, test_data, test_label, data_column_count, total_data_lines

    #train_size == 0 means that take all lines minus test lines
    def get_data(self, train_size, test_size, file_name):
        train_indexes, test_indexes = \
            self.get_indexes(train_size, test_size, file_name)

        return self.get_data_by_indexes(file_name, train_indexes, test_indexes)

    def get_time_data(self):
        train_size = 200000
        test_size = 100
        train_data, train_label, test_data, test_label, columns, lines = \
            self.get_data(train_size,
                          test_size,
                          self.variables.train_data_time)
        return train_data, train_label, test_data, test_label, columns, lines

    def get_combined_data(self):
        train_size = 0
        test_size = 50
        train_data, train_label, test_data, test_label, columns, lines  = \
            self.get_data(train_size,
                          test_size,
                          self.variables.trainDataAddressMultiple)
        return train_data, train_label, test_data, test_label, columns, lines

    def get_test_combined(self):
        return self.load_data(self.variables.testDataAddressMultiple)

    def get_address_data(self, limit, file_nr):
        train_size = 19940
        test_size = 50
        train_data, train_label, test_data, test_label, columns, lines  = \
            self.get_data(train_size,
                          test_size,
                          self.variables.train_data_address(limit, file_nr))
        return train_data, train_label, test_data, test_label, columns, lines

    def get_test_address(self, limit, file_nr):
        return self.load_data(self.variables.test_data_address(limit, file_nr))

    def get_coords_data(self):
        train_size = 200000
        test_size = 16
        train_data, train_label, test_data, test_label, columns, lines  = \
            self.get_data(train_size,
                          test_size,
                          self.variables.train_data_coord)
        return train_data, train_label, test_data, test_label, columns, lines

    def get_test_coord(self):
        return self.load_data(self.variables.test_data_coord)

    def load_data(self, data_file_path):
        with open(data_file_path, 'r') as csvfile:
            all_rows = self.get_array_of_lines(csvfile)
            return self.rows_to_data_and_category(all_rows)

    @staticmethod
    def get_array_of_lines(csvfile):
        reader = csv.reader(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
        line_number = 0
        all_rows = []
        for row in reader:
            if line_number % 200000 == 0:
                print(line_number)
            all_rows.insert(line_number, row)
            line_number += 1
        return all_rows

    @staticmethod
    def rows_to_data_and_category(all_rows):
        category_type = "categoryIdx"
        id_type = "id"

        def get_indexes_matrix(header, total_data_lines):
            is_category = header[0].startswith(category_type)
            is_id = header[0].startswith(id_type)
            if is_category:
                one_hot_category = np.zeros((total_data_lines, 39), dtype=np.int32)
                return one_hot_category, category_type
            elif is_id:
                ids = np.zeros((total_data_lines, 1), dtype=np.int32)
                return ids, id_type

        data_column_count = len(all_rows[1]) - 1
        total_data_lines = len(all_rows) - 1
        data = np.zeros((total_data_lines, data_column_count), dtype=np.float)

        header = all_rows[0]
        first_column_indexes, type_header = get_indexes_matrix(header, total_data_lines)

        line_number = 0
        for row_index in range(1, len(all_rows)):
            row = all_rows[row_index]
            if line_number % 200000 == 0:
                print(line_number)

            # first line should be header. First column is either category index (train) or id (test)
            for x in range(1, data_column_count + 1):
                data[line_number, x - 1] = float(row[x])

            if type_header == id_type:
                first_column_indexes[line_number] = int(row[0])
            elif type_header == category_type:
                first_column_indexes[line_number][int(row[0])] = 1
            line_number += 1

        return data, first_column_indexes, data_column_count, total_data_lines
