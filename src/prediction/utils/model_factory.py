from keras import backend as K
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers import Merge


class ModelFactory(object):
    def __init__(self):
        print("init ModelFactory")

    def get_coord_and_time(self, columns_time, columns_coord):
        coords_model = self.get_coords_model(columns_coord)
        time_model = self.get_time_model(columns_time)
        merged = Merge([coords_model, time_model], mode='concat')
        final_model = Sequential()
        final_model.add(merged)
        final_model.add(Dense(39, activation='softmax'))
        final_model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['accuracy'])
        return final_model

    def get_time_model(self, input_dim):
        return self.get_model(False, input_dim, 20)

    def get_time_pred_model(self, input_dim):
        return self.get_model(True, input_dim, 10)

    def get_coords_model(self, input_dim):
        return self.get_model(False, input_dim, 10)

    def get_coords_pred_model(self, input_dim):
        return self.get_model(True, input_dim, 10)

    def get_address_pred_model(self, input_dim):
        return self.get_model(True, input_dim, 100)

    def get_combined_pred_model(self, input_dim):

        return self.get_multi_layer_model(True, input_dim, 400, 100, 60)

    @staticmethod
    def get_model(activation, input_dim, first_hidden_size):
        model = Sequential()
        model.add(Dense(first_hidden_size, input_dim=input_dim, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(39, activation='relu'))
        if activation:
            model.add(Activation(K.softmax))
            model.compile(optimizer='rmsprop',
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])
        return model

    @staticmethod
    def get_multi_layer_model(activation, input_dim, first_hidden_size, second_hidden_size, third_hidden_size):
        model = Sequential()
        model.add(Dense(first_hidden_size, input_dim=input_dim, activation='relu'))
        model.add(Dropout(0.1))
        model.add(Dense(second_hidden_size, input_dim=input_dim, activation='relu'))
        #model.add(Dropout(0.1))
        model.add(Dense(third_hidden_size, input_dim=input_dim, activation='relu'))
        model.add(Dense(39))
        if activation:
            model.add(Activation(K.softmax)) #todo: test different activations
            #http://keras.io/optimizers/#rmsprop
            model.compile(optimizer='adam',
                          loss='categorical_crossentropy',
                          metrics=['accuracy'])
        return model

    @staticmethod
    def get_boosted_merge_model():
        model = Sequential()
        model.add(Dense(78, input_dim=78, activation='relu'))
        model.add(Dropout(0.1))
        model.add(Dense(50, activation='relu'))
        model.add(Dropout(0.1))
        model.add(Dense(39, activation='relu'))
        model.add(Activation(K.softmax)) #todo: test different activations
        model.compile(optimizer='adam',
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])
        return model
