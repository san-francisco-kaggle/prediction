class LocalVariables(object):
    def __init__(self):
        print("LocalVariables init")

    idea_base = "/Users/timonikkila/IdeaProjects"
    scala_project_base_path = idea_base + "/sf_kaggle/generatedData"
    python_project_base_path = idea_base + "/sf_crm_p"
    train_file = scala_project_base_path + "/trainClean.csv"

    train_data_coord = scala_project_base_path + '/trainCoord.csv'
    test_data_coord = scala_project_base_path + '/testCoord.csv'
    coordinates_weights = python_project_base_path + "/weights/coordinates.hdf5"
    prediction_raw = scala_project_base_path + '/predictionRaw.csv'
    predictions_vec = scala_project_base_path + '/predictionsVec.csv'

    train_data_time = scala_project_base_path + '/trainTime.csv'
    test_data_time = scala_project_base_path + '/testTime.csv'

    time_weights = python_project_base_path + "/weights/time.hdf5"
    combined_weights = python_project_base_path + '/weights/combined.hdf5'
    time_and_coord_weights = python_project_base_path + "/weights/timeAndCoord.hdf5"

    boostingFold = "/boosting"
    boosting_indexes = scala_project_base_path + boostingFold + "/indexes.csv"
    boost_branch_all_weights = scala_project_base_path + boostingFold + '/boost_branch_all_weights.hdf5'
    boost_branch_misfit_weights = scala_project_base_path + boostingFold + '/boost_branch_misfit_weights.hdf5'
    boost_merge_weights = scala_project_base_path + boostingFold + '/boost_merge_weights.hdf5'

    addressFolder = "/address"
    trainDataAddressMultiple = scala_project_base_path + addressFolder + "/trainMultipleOneHot.csv"
    testDataAddressMultiple = scala_project_base_path + addressFolder + "/testMultipleOneHot.csv"

    def train_data_combined(self, file_nr):
        return self.scala_project_base_path + '/trainCombined' + str(file_nr) + '.csv'

    def test_data_combined(self, file_nr):
        return self.scala_project_base_path + '/testCombined' + str(file_nr) + '.csv'

    def train_data_address(self, limit, file_nr):
        return self.scala_project_base_path + self.addressFolder + '/trainAddress' + str(limit) + '_' + str(file_nr) + '.csv'

    def test_data_address(self, limit, file_nr):
        return self.scala_project_base_path + self.addressFolder + '/test' + str(limit) + '_' + str(file_nr) + '.csv'

    def address_weights(self, limit):
        return self.python_project_base_path + '/weights/address' + str(limit) + '.hdf5'

    def address_prediction(self, limit):
        return self.python_project_base_path + self.addressFolder + "prediction" + str(limit) + '.csv'

