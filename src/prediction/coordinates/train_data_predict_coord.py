import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory

data_factory = DataFactory()
train_data, train_label, test_data, test_label, columns, lines = data_factory.get_coords_data()

variables = LocalVariables()
modelFactory = ModelFactory()
model = modelFactory.get_coords_pred_model(columns)
model.load_weights(variables.coordinates_weights)

loss_and_metrics = model.evaluate(test_data, test_label, batch_size=16)
print(loss_and_metrics)

classes = model.predict_classes(test_data, batch_size=32)
print("predicted classes")
print(classes)
print("true classes")
print(np.argmax(test_label, axis=1))
print("-----coordinates------")
print(train_data[0:5])
print("-----probabilities------")
proba = model.predict_proba(test_data, batch_size=32)
print(proba[0:5])
