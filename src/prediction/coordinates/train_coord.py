# #http://dedalus-project.readthedocs.org/en/latest/machines/mac_os/mac_os.html
import numpy as np

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory
from prediction.utils.data_factory import DataFactory

np.random.seed(1337)  # for reproducibility

data_factory = DataFactory()
train_data, train_label, test_data, test_label, columns, lines = data_factory.get_coords_data()
modelFactory = ModelFactory()
model = modelFactory.get_coords_pred_model(columns)

variables = LocalVariables()
early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
checkpoint = ModelCheckpoint(variables.coordinates_weights, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')
model.fit(train_data, train_label, nb_epoch=1, batch_size=32, validation_split=0.5, callbacks=[checkpoint, early_stop])

loss_and_metrics = model.evaluate(test_data, test_label, batch_size=32)
print(loss_and_metrics)

