import numpy as np

from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory
from prediction.utils.prediction_service import PredictionService

data_factory = DataFactory()
test_data, ids, columns, lines = data_factory.get_test_coord()

variables = LocalVariables()
modelFactory = ModelFactory()
model = modelFactory.get_coords_pred_model(columns)
model.load_weights(variables.coordinates_weights)

proba = model.predict_proba(test_data, batch_size=32)

predictionService = PredictionService
predictionService.save_predictions(ids, proba)
