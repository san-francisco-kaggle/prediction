import numpy as np
import gc

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory
from prediction.utils.data_factory import DataFactory
import time


class TrainCombined(object):

    modelFactory = ModelFactory()
    variables = LocalVariables()
    data_factory = DataFactory()

    def train_by(self, data, labels, weights_path, column_count):

        start_time = time.time()
        np.random.seed(1337)  # for reproducibility

        model = self.modelFactory.get_combined_pred_model(column_count)

        early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
        checkpoint = ModelCheckpoint(weights_path, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')
        gc.collect()
        model.fit(data, labels, nb_epoch=5, batch_size=32, validation_split=0.1, callbacks=[checkpoint, early_stop])

        classes = model.predict_classes(data, batch_size=32)
        print("--- %s seconds. total ---" % (time.time() - start_time))
        return classes

    def train(self):

        start_time = time.time()
        np.random.seed(1337)  # for reproducibility

        train_data, train_label, test_data, test_label, columns, lines = self.data_factory.get_combined_data()
        print("--- %s seconds. data loaded ---" % (time.time() - start_time))

        model = self.modelFactory.get_combined_pred_model(columns)

        early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
        checkpoint = ModelCheckpoint(self.variables.combined_weights, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')
        model.fit(train_data, train_label, nb_epoch=1, batch_size=32, validation_split=0.1, callbacks=[checkpoint, early_stop])

        loss_and_metrics = model.evaluate(test_data, test_label, batch_size=32)
        print(loss_and_metrics)

        print("--- %s seconds. total ---" % (time.time() - start_time))

#TrainCombined().train()
