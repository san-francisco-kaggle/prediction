import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory

limit = 100

data_factory = DataFactory()
train_data, train_label, test_data, test_label, columns, lines = data_factory.get_combined_data(0)

variables = LocalVariables()
modelFactory = ModelFactory()
model = modelFactory.get_combined_pred_model(columns)
model.load_weights(variables.combined_weights)

loss_and_metrics = model.evaluate(test_data, test_label, batch_size=16)
print(loss_and_metrics)

classes = model.predict_classes(test_data, batch_size=32)
print("predicted classes")
print(classes)
print("true classes")
print(np.argmax(test_label, axis=1))



