import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory
from prediction.utils.prediction_service import PredictionService

np.random.seed(1337)  # for reproducibility

#todo: predict vai predict_class?

variables = LocalVariables()
modelFactory = ModelFactory()
data_factory = DataFactory()

#todo: lot of dublicate

test_data, ids, column_count, data_lines = data_factory.get_test_combined()
model = modelFactory.get_combined_pred_model(column_count)
model.load_weights(variables.combined_weights)

proba = model.predict_proba(test_data, batch_size=32)

predictionService = PredictionService
predictionService.save_predictions(ids, proba)

