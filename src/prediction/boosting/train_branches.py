import numpy as np

from prediction.utils.data_factory import DataFactory
from prediction.variables import LocalVariables
from prediction.combined.train_combined import TrainCombined

class BoostingBranches(object):

    @staticmethod
    def getMissFits(train_data, train_label, first_branch_weights, column_count):
        train_combined = TrainCombined()
        classes = train_combined.train_by(train_data, train_label, first_branch_weights, column_count)

        misfit_data_max = np.zeros((len(train_data), column_count), dtype=np.float)
        misfit_labels_max = np.zeros((len(train_data), 39), dtype=np.int32)
        misfit_size = 0
        for ind in range(0, len(train_label)):
            if np.argmax(train_label[ind]) != classes[ind]:
                misfit_data_max[misfit_size] = train_data[ind]
                misfit_labels_max[misfit_size] = train_label[ind]
                misfit_size += 1

        misfit_data = misfit_data_max[0:misfit_size]
        misfit_labels = misfit_labels_max[0:misfit_size]
        return misfit_data, misfit_labels

    def train(self, train_data, train_label, first_branch_weights, second_branch_weights, column_count):
        misfit_data, misfit_labels = self.getMissFits(train_data, train_label, first_branch_weights, column_count)
        train_combined = TrainCombined()
        train_combined.train_by(misfit_data, misfit_labels, second_branch_weights, column_count)





