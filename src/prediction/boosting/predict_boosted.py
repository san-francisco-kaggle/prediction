import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory
from prediction.utils.prediction_service import PredictionService

np.random.seed(1337)

variables = LocalVariables()
modelFactory = ModelFactory()
data_factory = DataFactory()

#todo: lot of dublicate

test_data, ids, column_count, data_lines = data_factory.get_test_combined()

firstModel = modelFactory.get_combined_pred_model(column_count)
firstModel.load_weights(variables.boost_branch_all_weights)
firstProba = firstModel.predict_proba(test_data, batch_size=32)

secondModel = modelFactory.get_combined_pred_model(column_count)
secondModel.load_weights(variables.boost_branch_misfit_weights)
secondProba = secondModel.predict_proba(test_data, batch_size=32)

mergedProba = np.concatenate((firstProba, secondProba), axis=1)

model = modelFactory.get_boosted_merge_model()

proba = model.predict_proba(mergedProba, batch_size=32)
predictionService = PredictionService
predictionService.save_predictions(ids, proba)
