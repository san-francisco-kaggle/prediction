import numpy as np

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory
from prediction.utils.data_factory import DataFactory
from  prediction.boosting.train_branches import BoostingBranches
import time
from keras import backend as K
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers import merge, Input

variables = LocalVariables()
data_factory = DataFactory()

#Indexes
train_data, train_label, test_data, test_label, columns, lines  = \
    data_factory.get_data(0,0,variables.trainDataAddressMultiple)
all_lines_size =np.random.choice(lines, lines, replace=False)

#train lower brances
trainBranches = BoostingBranches()

branch_training_indexes = all_lines_size[0:int(len(all_lines_size)/2)]
merge_training_indexes = all_lines_size[int(len(all_lines_size)/2):]

branch_data = train_data[branch_training_indexes]
branch_label = train_label[branch_training_indexes]

merge_data = train_data[merge_training_indexes]
merge_label = train_label[merge_training_indexes]

trainBranches.train(branch_data,
                    branch_label,
                    variables.boost_branch_all_weights,
                    variables.boost_branch_misfit_weights,
                    columns)

modelFactory = ModelFactory()

firstModel = modelFactory.get_combined_pred_model(columns)
firstModel.load_weights(variables.boost_branch_all_weights)
firstProba = firstModel.predict_proba(merge_data, batch_size=32)

secondModel = modelFactory.get_combined_pred_model(columns)
secondModel.load_weights(variables.boost_branch_misfit_weights)
secondProba = secondModel.predict_proba(merge_data, batch_size=32)

mergedProba = np.concatenate((firstProba, secondProba), axis=1)

model = modelFactory.get_boosted_merge_model()

early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
checkpoint = ModelCheckpoint(variables.boost_merge_weights, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')
model.fit(mergedProba, merge_label, nb_epoch=5, batch_size=32, validation_split=0.1, callbacks=[checkpoint, early_stop])

loss_and_metrics = model.evaluate(mergedProba[1:60], merge_label[1:60], batch_size=32)
print(loss_and_metrics)

