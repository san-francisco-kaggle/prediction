import numpy as np
from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory

variables = LocalVariables()
data_factory = DataFactory()
train_labels, train_data_time, train_data_coord, test_labels, test_data_time, test_data_coord, columns_time, columns_coord = \
    data_factory.get_coords_and_time()

modelFactory = ModelFactory()
model = modelFactory.get_coord_and_time(columns_time, columns_coord)
model.load_weights(variables.time_and_coord_weights)

loss_and_metrics = model.evaluate([test_data_coord, test_data_time], test_labels, batch_size=16)
print(loss_and_metrics)

classes = model.predict_classes([test_data_coord, test_data_time], batch_size=32)
print("predicted classes")
print(classes)
print("true classes")
print(np.argmax(test_labels, axis=1))
print("-----probabilities------")
proba = model.predict_proba([test_data_coord, test_data_time], batch_size=32)
print(proba[0:5])


