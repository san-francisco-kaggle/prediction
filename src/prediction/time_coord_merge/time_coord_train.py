import numpy as np

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory
from prediction.utils.data_factory import DataFactory

np.random.seed(1337)  # for reproducibility

variables = LocalVariables()
data_factory = DataFactory()
train_labels, train_data_time, train_data_coord, test_labels, test_data_time, test_data_coord, columns_time, columns_coord = \
    data_factory.get_coords_and_time()

modelFactory = ModelFactory()
final_model = modelFactory.get_coord_and_time(columns_time, columns_coord)
early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
checkpoint = ModelCheckpoint(variables.time_and_coord_weights,
                               monitor='val_loss',
                               verbose=0,
                               save_best_only=True,
                               mode='auto')
final_model.fit([train_data_coord, train_data_time],
                train_labels,
                nb_epoch=2,
                batch_size=32,
                validation_split=0.1,
                callbacks=[checkpoint, early_stop])
