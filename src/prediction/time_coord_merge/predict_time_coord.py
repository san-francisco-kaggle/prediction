from prediction.utils.model_factory import ModelFactory
from prediction.variables import LocalVariables
from prediction.utils.data_factory import DataFactory

variables = LocalVariables()

data_factory = DataFactory()
ids, data_lines, coord_data, coord_column_count, time_data, time_column_count = \
    data_factory.get_test_coord_and_time()

model_factory = ModelFactory()
model = model_factory.get_coord_and_time(time_column_count, coord_column_count)
model.load_weights(variables.time_and_coord_weights)

classes = model.predict_classes([coord_data, time_data], batch_size=32)
print("predicted classes")
print(classes[100:150])
print("true classes")

if len(ids) != len(classes):
    raise NameError('Ids and classes must be column to column match with each other')

with open(variables.prediction_raw, 'w') as f:
    f.write("id;categoryIdx\n")
    total_size = len(ids)

    for x in range(0, total_size):
        f.write(str(ids[x][0]) + ";" + str(classes[x]) + "\n")
f.closed
