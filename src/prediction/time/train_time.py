import numpy as np

from keras.callbacks import ModelCheckpoint, EarlyStopping
from prediction.utils.data_factory import DataFactory
from prediction.variables import LocalVariables
from prediction.utils.model_factory import ModelFactory

np.random.seed(1337)

data_factory = DataFactory()
train_data, train_label, test_data, test_label, columns, lines = data_factory.get_time_data()

modelFactory = ModelFactory()
model = modelFactory.get_time_pred_model(columns)

variables = LocalVariables()

early_stop = EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')
checkpoint = ModelCheckpoint(variables.time_weights, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')
model.fit(train_data, train_label, nb_epoch=3, batch_size=32, validation_split=0.5, callbacks=[checkpoint, early_stop])

loss_and_metrics = model.evaluate(test_data, test_label, batch_size=32)
print(loss_and_metrics)
